extern crate chrono;
extern crate postgres;

mod moon;

use chrono::NaiveDateTime;
use postgres::{Connection, TlsMode};
use postgres::rows::{Row, Rows};

use moon::good;

struct Person {
  id: String,
  surname: String,
  name: String,
  patronymic: String,
  birth_date: Option<NaiveDateTime>,
}

fn main() {

  // user : probe_rust    pass: 222
  // database : rust_hello

  //let args: Vec<_> = std::env::args().collect(); 000000\

  good();

  let str1: String = From::from("foo");

  print!("{}", str1);

  let con = Connection::connect(
    "postgres://probe_rust:222@localhost:5432/rust_hello", TlsMode::None,
  ).unwrap();

  let sql = "SELECT id, surname, name, patronymic, birth_date FROM person WHERE id = $1";

  let id = "i1";

  let result_list: &Rows = &con.query(sql, &[&id]).unwrap();

  for row in result_list {
    let u: Row = row;

    let p = Person {
      id: u.get(0),
      surname: u.get(1),
      name: u.get(2),
      patronymic: u.get(3),
      birth_date: u.get(4),
    };

    println!("Found person {}: {} {} {} - {:?}", p.id, p.surname, p.name, p.patronymic, p.birth_date);
  }
}
